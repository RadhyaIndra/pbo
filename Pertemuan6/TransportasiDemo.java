import Transportasi.*;

public class TransportasiDemo {
    public static void main(String[] args) {
        // membuat objek Mobil dan memanggil methodnya
        Mobil mobil = new Mobil();
        mobil.hidupkanMobil();
        mobil.ubahGigi();
            // membuat objek Bicycle dan memanggil methodnya
        Bicycle sepeda = new Bicycle();
        sepeda.naikSepeda();
        sepeda.ubahGigi();
        sepeda.turunSepeda();

    }
}