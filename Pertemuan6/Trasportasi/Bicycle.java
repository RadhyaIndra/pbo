package Transportasi;

import java.util.Scanner;

import javax.print.DocFlavor.STRING;

public class Bicycle {
    private String warna;
    private int th_produksi;
    private int gear = 0;
    
    public void naikSepeda(){
        System.out.println("Angkat kaki dan duduk di atas sadel ngek");
    }

    public void turunSepeda(){
        System.out.println("Angkat kaki dan turun dari sadel brak");
    }

    public void ubahGigi(){
        Scanner data = new Scanner(System.in);
        int gear = 0;
        boolean lanjut = true;
        
        while (lanjut) {
            System.out.print("Mau ganti gear motor? (y/n) ");
            String pilihan = data.next();
        
            if (pilihan.equalsIgnoreCase("y")) {
                System.out.print("Masukkan gear berapa: ");
                gear = data.nextInt();
                System.out.println("Sekarang menggunakan gigi " + gear);
            } else if (pilihan.equalsIgnoreCase("n")) {
                lanjut = false;
            } else {
                System.out.println("Pilihan tidak valid!");
            }
        }
    }
}