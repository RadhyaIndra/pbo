package Transportasi;

import java.util.Scanner;

public class Mobil {
    private String warna;
    private int th_produksi;
    private int gear = 0;

    public void hidupkanMobil(){
        System.out.println("Brenggggg mobil nyala ");
    }

    public void matikanMobil(){
        System.out.println("car is ded ");
    }

    public void ubahGigi(){
        Scanner data = new Scanner(System.in);
        int gear = 0;
        boolean lanjut = true;
        
        while (lanjut) {
            System.out.print("Mau ganti gear mobil? (y/n) ");
            String pilihan = data.next();
        
            if (pilihan.equalsIgnoreCase("y")) {
                System.out.print("Masukkan gear berapa: ");
                gear = data.nextInt();
                System.out.println("Sekarang menggunakan gigi " + gear);
            } else if (pilihan.equalsIgnoreCase("n")) {
                lanjut = false;
            } else {
                System.out.println("Pilihan tidak valid!");
            }
        }
    }
}