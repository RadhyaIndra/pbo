import java.util.Scanner;


public class Program_Kalkulator{
	String nim;
	String nama;
	float 	nilaitgs,
			nilaiuts,
			nilaiuas,
			nilaiakhir;
	

	public void data_dan_nilai (	String nim,
									String nama,
									float nilaitgs,
									float nilaiuts,
									float nilaiuas){
	this.nim=nim;
	this.nama=nama;
	this.nilaitgs=nilaitgs;
	this.nilaiuts=nilaiuts;
	this.nilaiuas=nilaiuas;
	}

	void view(){
		System.out.println("===========================");
		System.out.println("PROGRAM KALKULATOR NILAI");
		System.out.println("===========================");
		System.out.println("NIM\t\t:"+this.nim);
		System.out.println("NAMA\t\t:"+this.nama);
		System.out.println("---------------------------");
		System.out.println("Nilai TUGAS\t:"+this.nilaitgs);
		System.out.println("Nilai UTS\t:"+this.nilaiuts);
		System.out.println("Nilai UAS\t:"+this.nilaiuas);
	}

	void hitung()
	{
		nilaiakhir=((this.nilaitgs)*30/100+
					(this.nilaiuts)*35/100+
					(this.nilaiuas)*35/100);
		System.out.println("---------------------------");
		System.out.println("Nilai Akhir\t:"+this.nilaiakhir);
		System.out.println("---------------------------");
		if (nilaiakhir>=85)
        {
            System.out.println("Nilai Huruf: A\n");
        }else if (nilaiakhir>=80)
        {
            System.out.println("Nilai Huruf: AB\n");
        }else if (nilaiakhir>=70)
        {
            System.out.println("Nilai Huruf: B\n");
        }else if (nilaiakhir>=60)
        {
            System.out.println("Nilai Huruf: C\n");
        }else if (nilaiakhir>=50)
        {
            System.out.println("Nilai Huruf: D\n");
        }else
        {
            System.out.println("Nilai Huruf: E\n");
        }
		
	}
}
