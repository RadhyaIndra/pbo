import java.util.Scanner;

public class Nilai{
	public static void main(String[] args)
	{
		String jawab = "tidak";
		boolean running = true;
		Scanner sc = new Scanner(System.in);
		Program_Kalkulator nilai=new Program_Kalkulator();

		do {
			System.out.println("Input Nama  :");
			nilai.nama = sc.nextLine();
			System.out.println("Input Nim   :");
			nilai.nim = sc.nextLine();
			System.out.println("Input Nilai Tugas   :");
			nilai.nilaitgs = sc.nextFloat();
			System.out.println("Input Nilai Uts :");
			nilai.nilaiuts = sc.nextFloat();
			System.out.println("Input Nilai Uas :");
			nilai.nilaiuas = sc.nextFloat();
	
			//nilai.data_dan_nilai("A11202113537","Wisesa Sat S",86,80,90);
			nilai.view();
			nilai.hitung();


			System.out.println("Apakah anda ingin keluar?");
            System.out.print("Jawab [ya/tidak]> ");

            jawab = sc.next();

            // cek jawabnnya, kalau ya maka berhenti mengulang
            if( jawab.equalsIgnoreCase("ya") ){
                running = false;
            }

        } while (running);
	}
}