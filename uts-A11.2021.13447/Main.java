import java.util.Scanner;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ArrayList<Mahasiswa> listMahasiswa = new ArrayList<Mahasiswa>();
        
        // input data mahasiswa
        System.out.print("Masukkan jumlah mahasiswa: ");
        int jumlahMahasiswa = scanner.nextInt();
        scanner.nextLine();
        
        for (int i = 0; i < jumlahMahasiswa; i++) {
            System.out.println("Mahasiswa " + (i+1));
            System.out.print("Masukkan NIM: ");
            String nim = scanner.nextLine();
            System.out.print("Masukkan nama: ");
            String nama = scanner.nextLine();
            System.out.print("Masukkan semester: ");
            int semester = scanner.nextInt();
            scanner.nextLine(); 
            System.out.print("Masukkan usia: ");
            int usia = scanner.nextInt();
            scanner.nextLine();
            System.out.print("Masukkan jumlah mata kuliah: ");
            int jumlahMataKuliah = scanner.nextInt();
            scanner.nextLine();
            String[] krs = new String[jumlahMataKuliah];
            for (int j = 0; j < jumlahMataKuliah; j++) {
                System.out.print("Masukkan nama mata kuliah ke-" + (j+1) + ": ");
                krs[j] = scanner.nextLine();
            }
            System.out.print("Masukkan jumlah nilai: ");
            int jumlahNilai = scanner.nextInt();
            scanner.nextLine();
            int[] nilai = new int[jumlahNilai];
            for (int j = 0; j < jumlahNilai; j++) {
                System.out.print("Masukkan nilai ke-" + (j+1) + ": ");
                nilai[j] = scanner.nextInt();
                scanner.nextLine();
            }
            listMahasiswa.add(new Mahasiswa(nim, nama, semester, usia, krs, nilai));
            System.out.println();
        }
        
        // Tampilkan data mahasiswa yang telah diinputkan
        System.out.println("Daftar mahasiswa yang telah diinputkan:");
        for(int i = 0; i < listMahasiswa.size(); i++){
            Mahasiswa mahasiswa = listMahasiswa.get(i);
            float rataNilai = mahasiswa.hitungRataNilai(mahasiswa.getNilai());
            mahasiswa.infoMahasiswa();
            mahasiswa.infoKrsMahasiswa();
            System.out.println("Rata-rata nilai: " + rataNilai);
            System.out.println();
        }
    }
}
