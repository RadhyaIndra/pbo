public class Mahasiswa{
    String nim;
    String nama;
    int semester;
    int usia;
    String[] krs;
    int[] nilai;

    public Mahasiswa(String nim, String nama, int semester, int usia, String[] krs, int[] nilai) {
        this.nim = nim;
        this.nama = nama;
        this.semester = semester;
        this.usia = usia;
        this.krs = krs;
        this.nilai = nilai;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNim() {
        return nim;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNama() {
        return nama;
    }

    public void setSemester(int semester) {
        this.semester = semester;
    }

    public int getSemester() {
        return semester;
    }

    public void setUsia(int usia) {
        this.usia = usia;
    }

    public int getUsia() {
        return usia;
    }

    public void setKrs(String[] krs) {
        this.krs = krs;
    }

    public String[] getKrs() {
        return krs;
    }

    public int[] getNilai() {
        return nilai;
    }

    public float hitungRataNilai(int[] nilai){
        int totalNilai = 0;
        for(int i = 0; i < nilai.length; i++){
            totalNilai += nilai[i];
        }
        return (float)totalNilai/nilai.length;
    }

    public void infoMahasiswa(){
        System.out.println("NIM: " + nim);
        System.out.println("Nama: " + nama);
        System.out.println("Semester: " + semester);
        System.out.println("Usia: " + usia);
    }

    public void infoKrsMahasiswa(){
        System.out.println("Daftar KRS: ");
        for(int i = 0; i < krs.length; i++){
            System.out.println("- " + krs[i]);
        }
    }
}   
