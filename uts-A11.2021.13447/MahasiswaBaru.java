class MahasiswaBaru extends Mahasiswa{
    String asalSekolah;

    public void setAsalSekolah(String asalSekolah) {
        this.asalSekolah = asalSekolah;
    }

    public int getAsalSekolah() {
        return asalSekolah;
    }

    boolean ikutOspek(){

    }

    void infoMahasiswa(){
        super.infoMahasiswa(); // memanggil method infoMahasiswa pada superclass
        System.out.println("Asal Sekolah: " + asalSekolah);
    }
}