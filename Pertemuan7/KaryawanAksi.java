public class KaryawanAksi {
    public static void main(String[] args) {
        KaryawanTetap karyawanTetap = new KaryawanTetap("Andi", "Laki-laki", 30, "Manajer", 4000000);
        double totalGaji = karyawanTetap.hitungTotalGaji(200000);
        // mencetak karyawan tetap
        karyawanTetap.cetak();
        System.out.println("Total gaji: " + totalGaji);

        KaryawanKontrak karyawanKontrak = new KaryawanKontrak("Lina", "Perempuan", 25, "Marketing", 75000);
        double totalUpah = karyawanKontrak.hitungTotalUpah(20, 1000000);
        // mencetak karyawan kontrak
        karyawanKontrak.cetak();
        System.out.println("Total upah: " + totalUpah);
    }
}
