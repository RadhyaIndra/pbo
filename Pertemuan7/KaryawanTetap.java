public class KaryawanTetap extends Karyawan{
    int gajiPokok;

    public KaryawanTetap(String nama, String jenisKelamin, int umur, String jabatan, int gajiPokok) {
        super(nama, jenisKelamin, umur, jabatan);
        this.gajiPokok = gajiPokok;
    }

    public double hitungTotalGaji(double tunjanganAnak) {
        return this.gajiPokok + tunjanganAnak;
    }

    void cetak(){
        System.out.println("Nama : " +nama);
        System.out.println("Jenis Kelamin : " +jenisKelamin);
        System.out.println("Umur : " +umur);
        System.out.println("Jabatan : " +jabatan);
    }
}
