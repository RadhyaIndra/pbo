public class Karyawan{
    String nama;
    String  jenisKelamin;
    int umur;
    String jabatan;

    public Karyawan(String nama, String jenisKelamin, int umur, String jabatan){
        this.nama = nama;
        this.jenisKelamin = jenisKelamin;
        this.umur = umur;
        this.jabatan = jabatan;
    }
}