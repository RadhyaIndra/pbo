public class KaryawanKontrak extends Karyawan {
    int upahHarian;

    public KaryawanKontrak(String nama, String jenisKelamin, int umur, String jabatan, int upahHarian) {
        super(nama, jenisKelamin, umur, jabatan);
        this.upahHarian = upahHarian;
    }

    public double hitungTotalUpah(int jumlahHariMasuk, double tunjanganAnak) {
        return (this.upahHarian * jumlahHariMasuk) + tunjanganAnak;
    }

    void cetak(){
        System.out.println("Nama : " +nama);
        System.out.println("Jenis Kelamin : " +jenisKelamin);
        System.out.println("Umur : " +umur);
        System.out.println("Jabatan : " +jabatan);
    }
}
